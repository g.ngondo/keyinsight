﻿using Keyinsight.core.communications.messages;
using Keyinsight.core.interfaces.communications;
using Keyinsight.core.interfaces.models;
using Keyinsight.core.models;
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Keyinsight.core.communications
{
    public abstract class WebSocketHandler : IWebSocketHandler
    {
        public IWebSocketConnectionManager ConnectionManager { get; set; }
        public MessageHandlerType HandlerType { get; set; }
        public IMessageQueue MessageQueue { get; set; }
        public WebSocketHandler(MessageHandlerType handlerType, IWebSocketConnectionManager connectionManager, IMessageQueue messageQueue)
        {
            HandlerType = handlerType;
            ConnectionManager = connectionManager;
            MessageQueue = messageQueue;
        }
        public virtual void OnConnected(WebSocket socket)
        {
            ConnectionManager.AddConnection(socket);
        }

        public async Task OnDisconnected(WebSocket socket)
        {
            Guid id = ConnectionManager.GetConnectionId(socket);
            await ConnectionManager.RemoveConnection(id);

        }
        public async Task SendMessageAsync(WebSocket socket, IMessage message)
        {
            if (socket.State != WebSocketState.Open)
                return;

            byte[] messageBody = ToByteArray(message);
            var segmentArray = new ArraySegment<byte>(messageBody,offset:0, count:messageBody.Length);
            
            await socket.SendAsync(buffer: segmentArray,
                                   messageType: WebSocketMessageType.Binary,
                                   endOfMessage: true,
                                   cancellationToken: CancellationToken.None);
        }

        public async Task SendMessageAsync(Guid id, IMessage message)
        {
            await SendMessageAsync(ConnectionManager.GetConnectionById(id), message);
        }

        public async Task SendMessageToAllAsync(IMessage message)
        {
            ICollection<Guid> keys = ConnectionManager.Connections.Keys;
            foreach (Guid guid in keys)
            {
                await SendMessageAsync(guid, message);
            }
        }
        public abstract Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer);

        public IMessage FromByteArray(byte[] data)
        {
            int rawsize;
            IMessage retobj;

            rawsize = Marshal.SizeOf(typeof(Message));
            if (rawsize > data.Length)
                return null;
            GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            IntPtr buffer = handle.AddrOfPinnedObject();
            retobj = (IMessage)Marshal.PtrToStructure(buffer, typeof(Message));
            handle.Free();
            return retobj;
        }

        public byte[] ToByteArray(IMessage message)
        {
            int rawsize;
           
            rawsize = Marshal.SizeOf(typeof(Message));
            byte[] rawdatas = new byte[rawsize];
            GCHandle handle = GCHandle.Alloc(rawdatas, GCHandleType.Pinned);
            IntPtr buffer = handle.AddrOfPinnedObject();
            Marshal.StructureToPtr(message, buffer, false);
            Marshal.Copy(rawdatas, 0, buffer, rawsize);
            handle.Free();
            
            return rawdatas;
        }

    }
}
