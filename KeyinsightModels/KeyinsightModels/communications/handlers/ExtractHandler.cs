﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Keyinsight.core.interfaces.communications;

namespace Keyinsight.core.communications.messages
{
    public class ExtractHandler : WebSocketHandler, IExtractHandler
    {
        public ExtractHandler(IWebSocketConnectionManager connectionManager, IMessageQueue messageQueue) : base(MessageHandlerType.ExtractHandler, connectionManager, messageQueue)
        {
        }
        public override void OnConnected(WebSocket socket)
        {
            base.OnConnected(socket);
        }
        public override Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            throw new NotImplementedException();
        }
    }
}
