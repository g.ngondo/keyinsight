﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Keyinsight.core.interfaces.communications;

namespace Keyinsight.core.communications.messages
{
    public class AnalysisHandler : WebSocketHandler, IAnalysisHandler
    {
        public AnalysisHandler(IWebSocketConnectionManager connectionManager, IMessageQueue messageQueue) : base(MessageHandlerType.AnalysisHandler, connectionManager, messageQueue)
        {
        }
        public override void OnConnected(WebSocket socket)
        {
            base.OnConnected(socket);
        }
        public override Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            throw new NotImplementedException();
        }
    }
}
