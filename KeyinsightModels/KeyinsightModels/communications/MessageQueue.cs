﻿using Keyinsight.core.interfaces.communications;
using System;
using System.Collections.Generic;
using System.Text;
using Keyinsight.core.interfaces.models;
using System.Threading;

namespace Keyinsight.core.communications
{
    public class MessageQueue : IMessageQueue
    {
        private readonly Queue<IMessage> queue;
        public IDictionary<Guid, IMessageSubscriber> Subscribers { get; set; }
        public int Size { get; set; }

        public MessageQueue()
        {
            queue = new Queue<IMessage>();
            Subscribers = new Dictionary<Guid, IMessageSubscriber>();
            Size = 0;
        }
        
        

        public void AddSubscriber(IMessageSubscriber subscriber)
        {
            Subscribers.Add(subscriber.Id, subscriber);
        }

        public IMessage Dequeue()
        {
            lock (queue)
            {
                while (queue.Count == 0)
                {
                    Monitor.Wait(queue);
                }
                IMessage item = queue.Dequeue();
                if (queue.Count == Size - 1)
                {
                    // wake up any blocked enqueue
                    Monitor.PulseAll(queue);
                }
                return item;
            }
        }

        public void Enqueue(IMessage obj)
        {
            lock (queue)
            {
                while (queue.Count >= Size)
                {
                    Monitor.Wait(queue);
                }
                queue.Enqueue(obj);
                if (queue.Count == 1)
                {
                    // wake up any blocked dequeue
                    Monitor.PulseAll(queue);
                }
            }
        }

        public void NotifySubscribers(IMessage message)
        {
            foreach (IMessageSubscriber sub in Subscribers.Values)
            {
                sub.Notify(message);
            }
        }

        public void RemoveSubscriber(Guid subscriberId)
        {
            Subscribers.Remove(subscriberId);
        }
    }
}
