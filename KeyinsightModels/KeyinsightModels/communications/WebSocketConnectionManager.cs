﻿using Keyinsight.core.interfaces.communications;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace Keyinsight.core.communications
{
    public class WebSocketConnectionManager : IWebSocketConnectionManager
    {
        public ConcurrentDictionary<Guid, WebSocket> Connections { get; set; }

        public WebSocketConnectionManager()
        {
            Connections = new ConcurrentDictionary<Guid, WebSocket>();
        }

        public void AddConnection(WebSocket socket)
        {
            Connections.TryAdd(CreateConnectionId(), socket);
        }

        public Guid CreateConnectionId()
        {
            return Guid.NewGuid();
        }

        public WebSocket GetConnectionById(Guid id)
        {
            return Connections.FirstOrDefault(p => p.Key == id).Value;
        }

        public Guid GetConnectionId(WebSocket socket)
        {
           return Connections.FirstOrDefault(p => p.Value == socket).Key;
        }

        public async Task RemoveConnection(Guid id)
        {
            bool removed = Connections.TryRemove(id, out WebSocket socket);
            await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed by Connection Manager", CancellationToken.None);
        }

        
    }
}
