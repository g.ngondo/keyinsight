﻿using Keyinsight.core.models;
using Keyinsight.core.interfaces.nodes;
using Keyinsight.core.interfaces.communications;
using System;

namespace Keyinsight.core.repositories
{
    public class BaseEntityRepository<T> : EntityRepository<T>
        where T: BaseEntity
    {
        public BaseEntityRepository(IBaseNodeContext context) : base(context)
        {
            Id = Guid.NewGuid();
        }
      
    }
}
