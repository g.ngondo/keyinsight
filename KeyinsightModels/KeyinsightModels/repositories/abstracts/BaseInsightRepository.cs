﻿using Keyinsight.core.interfaces.nodes;
using Keyinsight.core.interfaces.repositories;
using Keyinsight.core.models;

namespace Keyinsight.core.repositories
{
    public class BaseInsightRepository : BaseEntityRepository<BaseInsight>, IBaseInsightRepository
    {
        public BaseInsightRepository(IBaseNodeContext context) : base(context)
        {
        }
    }
}
