﻿using Keyinsight.core.models;
using Keyinsight.core.interfaces.repositories;
using Keyinsight.core.interfaces.nodes;

namespace Keyinsight.core.repositories
{
    public class BaseSubjectRepository : BaseEntityRepository<BaseSubject>, IBaseSubjectRepository
    {
        public BaseSubjectRepository(IBaseNodeContext context) : base(context)
        {
        }
    }
}
