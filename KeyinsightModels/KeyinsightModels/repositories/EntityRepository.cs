﻿using Keyinsight.core.interfaces.communications;
using Keyinsight.core.interfaces.models;
using Keyinsight.core.interfaces.nodes;
using Keyinsight.core.interfaces.repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Keyinsight.core.repositories
{

    public class EntityRepository<E> : IEntityRepository<E>, IMessageSubscriber
        where E : class,IEntity
    {
        IBaseNodeContext _context;

        public EntityRepository(IBaseNodeContext context)
        {
            _context = context;
        }

        public RepositoryType RepositoryType { get; set; }
        public Guid Id { get; set; }
        public SubscriberStatus Status { get; set; }

        public void Add(E entity)
        {
            _context.Set<E>().Add(entity);
        }

        public IEnumerable<E> AllIncluding(params Expression<Func<E, object>>[] includeProperties)
        {
            IQueryable<E> query = _context.Set<E>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query.AsEnumerable();
        }

        public async Task Commit()
        {
          await _context.SaveChangesAsync();
        }

        public int Count()
        {
            return _context.Set<E>().Count();
        }

        public void Delete(E entity)
        {

            EntityEntry dbEntityEntry = _context.Set<E>().Remove(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public void DeleteWhere(Expression<Func<E, bool>> predicate)
        {
            IEnumerable<E> entities = _context.Set<E>().Where(predicate);

            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

        public IEnumerable<E> FindBy(Expression<Func<E, bool>> predicate)
        {
            return _context.Set<E>().Where(predicate);
        }

        public IEnumerable<E> GetAll()
        {
            IEnumerable<E> test = _context.Set<E>().AsEnumerable(); 
            return _context.Set<E>().AsEnumerable();
        }

        public E GetSingle(int id)
        {
            return _context.Set<E>().FirstOrDefault(x => x.Id == id);
        }

        public E GetSingle(Expression<Func<E, bool>> predicate)
        {
            return _context.Set<E>().FirstOrDefault(predicate);
        }

        public E GetSingle(Expression<Func<E, bool>> predicate, params Expression<Func<E, object>>[] includeProperties)
        {
            IQueryable<E> query = _context.Set<E>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.Where(predicate).FirstOrDefault();
        }

        public void Notify(IMessage message)
        {
            throw new NotImplementedException();
        }

        public void Update(E entity)
        {
            EntityEntry dbEntityEntry = _context.Set<E>().Update(entity);
            dbEntityEntry.State = EntityState.Modified;
        }
    }
}
