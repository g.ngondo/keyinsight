﻿using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    public class Keyword :  BaseSubject,IKeyword
    {
        public Keyword() : base(SubjectType.Keyword)
        {
            EntityName = "Keyword";
        }

        public string Text { get; set; }
       
    }
}