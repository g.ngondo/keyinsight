﻿using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    public class Certification : BaseSubject, ICertification
    {
        public Certification(CertificationType certType) : base(SubjectType.Certification)
        {
            CertificationType = certType;
            EntityName = "Certification";
        }

        public string CertificationName { get; set; }
        public CertificationType CertificationType { get; set; }
        public IDomain CertificationSkillDomain { get; set; }
    }
    public enum CertificationType
    {
        Degree,
        Certification,
    }
}
