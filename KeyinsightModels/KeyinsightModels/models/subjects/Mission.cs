﻿using Keyinsight.core.interfaces.models;
using System;

namespace Keyinsight.core.models
{
    public class Mission : BaseSubject, IMission
    {
        public Mission(MissionType missionType) : base(SubjectType.Mission)
        {
            MissionType = missionType;
            EntityName = "Mission";
        }
        public MissionType MissionType { get; set; }
        public string MissionName { get; set; }
        public string MissionDescription { get; set; }
        public string MissionDetail { get; set; }
        public DateTime MissionStartDate { get; set; }
        public DateTime MissionEndDate { get; set; }
        
    }
    public enum MissionType
    {
        NoType,
    }
}