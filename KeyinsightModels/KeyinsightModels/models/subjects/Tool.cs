﻿using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    public class Tool : BaseSubject, ITool
    {
        public Tool(ToolType toolType) : base(SubjectType.Tool)
        {
            ToolType = toolType;
            EntityName = "Tool";
        }
        public ToolType ToolType { get; set; }
        public string ToolName { get; set; }
        public Domain ToolSkillDomain { get; set; }
    }
    public enum ToolType
    {
        Technical,
        Functionnal,
    }
}
