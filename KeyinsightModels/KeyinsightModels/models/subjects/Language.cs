﻿using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    public class Language : BaseSubject, ILanguage
    {
        public Language() : base(SubjectType.Language)
        {
            EntityName = "Language";
        }

        public string LanguageName { get; set; }
    }
   
}
