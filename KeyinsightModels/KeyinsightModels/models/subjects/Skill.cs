﻿using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    public class Skill : BaseSubject, ISkill
    {
        public Skill() : base(SubjectType.Skill)
        {
            EntityName = "Skill";
        }

        public string SkillName { get; set; }
        public Domain SkillDomain { get; set; }
    }
  
}
