﻿using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    public class Location : BaseSubject, ILocation
    {
        public Location(LocationType locType) : base(SubjectType.Location)
        {
            LocationType = locType;
            EntityName = "Location";
        }
        public float LocationLatitude { get; set; }
        public float LocationLongitude { get; set; }
        public string LocationName { get; set; }
        public LocationType LocationType { get; set; }
    }
    public enum LocationType
    {
        Continent,
        Country,
        City,
        Address
    }
}