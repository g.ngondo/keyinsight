﻿using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    public class Category : BaseSubject, ICategory
    {
        public Category() : base(SubjectType.Category)
        {
            EntityName = "Category";
        }
        public string CategoryName { get; set; }
    }
}
