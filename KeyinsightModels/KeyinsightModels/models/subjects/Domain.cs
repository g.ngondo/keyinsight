﻿using Keyinsight.core.interfaces.models;
using System.Collections.Generic;

namespace Keyinsight.core.models
{
    public class Domain : BaseSubject, IDomain
    {
        public Domain(DomainType domainType) : base(SubjectType.Domain)
        {
            DomainType = domainType;
            EntityName = "Domain";
        }

        public string DomainName { get; set; }
        public DomainType DomainType { get; set; }
        public ICollection<ISkill> DomainSkills { get; set; }
        public ICollection<ITool> DomainTools { get; set; }
        public ICategory DomainCategory { get; set; }
        
    }
    public enum DomainType
    {
        Technical,
        Functionnal,
        Operational,
    }
    
}
