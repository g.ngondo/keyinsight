﻿using Keyinsight.core.interfaces.models;
using System;
using System.Runtime.InteropServices;


namespace Keyinsight.core.models
{

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct Message : IMessage
    {
        public Message(IBaseEntity body, IMessageDestination dest)
        {
            MessageBody = body;
            MessageTo = dest;
            MessageId = Guid.NewGuid();
        }
        public IBaseEntity MessageBody { get; set; }
        public IMessageDestination MessageTo { get; set; }
        public Guid MessageId { get; set; }
    }
    
}
