﻿using Keyinsight.core.interfaces.models;
using System;
using System.Collections.Generic;

namespace Keyinsight.core.models
{
    public class Training : BaseInsight, IBaseInsight<BaseSubject>
    {
        public Training() : base(InsightType.Training)
        {
            EntityName = "Training";
        }

        public string TrainingName { get; set; }
       public DateTime TrainingStartDate { get; set; }
       public DateTime TrainingEndDate { get; set; }
       public IOrganization TrainingOrganization { get; set; }
       public ICollection<IDomain> TrainingDomains { get; set; }
       public ICollection<ISkill> TrainingSkills { get; set; }
       public ICollection<ITool> TrainingTools { get; set; }
       public ICollection<ICertification> TrainingCertifications { get; set; }
    }

    
}