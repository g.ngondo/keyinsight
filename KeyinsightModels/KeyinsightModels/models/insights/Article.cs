﻿using System;
using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    public class Article : BaseInsight, IBaseInsight<BaseSubject>
    {
        public Article() : base(InsightType.Article)
        {
            EntityName = "Article";
        }

        public string ArticleName { get; set; }
        public DateTime ArticleDate { get; set; }
        public string ArticleContent { get; set; }
        public string ArticleURI { get; set; }
    }
}
