﻿using Keyinsight.core.interfaces.models;
using System;
using System.Collections.Generic;

namespace Keyinsight.core.models
{
    public class Job : BaseInsight, IBaseInsight<BaseSubject>
    {
        public Job(JobType jobType) : base(InsightType.Job)
        {
            JobType = jobType;
            EntityName = "Job";
        }
        public JobType JobType { get; set; }
        public IOrganization JobOrganization { get; set; }
        public ILocation JobLocation { get; set; }
        public DateTime JobStartDate { get; set; }
        public DateTime JobEndDate { get; set; }
        public string JobDetail { get; set; }
        public ICollection<IMission> JobMissions { get; set; }
        public ICollection<IDomain> JobDomains { get; set; }
        public ICollection<ISkill> JobSkills { get; set; }
        public ICollection<ITool> JobTools { get; set; }
    }
    public enum JobType
    {
        Job,
        StudentProject,
        Project,
    }
}
