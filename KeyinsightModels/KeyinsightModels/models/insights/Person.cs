﻿using Keyinsight.core.interfaces.models;
using System.Collections.Generic;

namespace Keyinsight.core.models
{
    public class Person : BaseInsight, IBaseInsight<BaseSubject>
    {
        public Person() : base(InsightType.Person)
        {
            EntityName = "Person";
        }

        public ICollection<ICv> PersonCvs { get; set; }
        public IOrganization PersonCurrentOrganization { get; set; }
        public PersonStatus PersonStatus { get; set; }
        public string PersonName { get; set; }
    }
    public enum PersonStatus
    {
        None,
    }
}
