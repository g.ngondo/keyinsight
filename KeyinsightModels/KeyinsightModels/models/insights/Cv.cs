﻿using Keyinsight.core.interfaces.models;
using System.Collections.Generic;

namespace Keyinsight.core.models
{
    public class Cv : BaseInsight, IBaseInsight<BaseSubject>
    {
        public Cv() : base(InsightType.Cv)
        {
            EntityName = "Cv";
        }

        public bool IsStandard { get; set; }
        public ICollection<ITraining> CvTrainings { get; set; }
        public ICollection<IJob> CvJobs { get; set; }
        public ICollection<ILanguage> CvLanguages { get; set; }
        public IPerson CvOwner { get; set; }

    }
}
