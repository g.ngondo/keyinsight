﻿using Keyinsight.core.interfaces.models;
using System.Collections.Generic;

namespace Keyinsight.core.models
{
    public class Organization : BaseInsight, IBaseInsight<BaseSubject>
    {
        public Organization(OrganizationType orgType) : base(InsightType.Organization)
        {

            OrganizationType = orgType;
            EntityName = "Organization";
        }

        public OrganizationType OrganizationType { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationLogo { get; set; }
        public ILocation OrganizationLocation { get; set; }
        public string OrganizationWebUrl { get; set; }
        public ICollection<IDomain> OrganizationDomains { get; set; }
        public ICollection<IJob> OrganizationJobs { get; set; }
        public ICollection<IPerson> OrganizationPersons { get; set; }
        public ICollection<ICertification> OrganizationCertifications { get; set; }
    }
   
    public enum OrganizationType
    {
        School,
        Company,
    }
}