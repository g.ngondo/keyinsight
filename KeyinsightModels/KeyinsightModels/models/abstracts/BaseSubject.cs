﻿using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    public class BaseSubject : BaseEntity, IBaseSubject
    {
        public BaseSubject() : base(EntityType.Subject)
        {
        }
        public BaseSubject(SubjectType subjectType) : base(EntityType.Subject)
        {
            SubjectType = subjectType;
        }
        public SubjectType SubjectType { get; set; }
    }
    

}
