﻿using Keyinsight.core.interfaces.models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;

namespace Keyinsight.core.models
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class BaseEntity : IBaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [Required]
        public EntityFlowStatus EntityStatus { get; set; }
        [Required]
        public EntityType EntityType { get; }
        public DateTime EntityExtractedDate { get; set; }
        public DateTime EntityAugmentedDate { get; set; }
        public DateTime EntityLoadedDate { get; set; }
        public string EntityName { get; set; }
        public string EntityValue { get; set; }
        public Guid EntityPublicId { get; set; }
        /*public BaseEntity()
        {
        }*/
        public BaseEntity(EntityType type) :
            base()
        {
            EntityExtractedDate = DateTime.Now;
            EntityStatus = EntityFlowStatus.Created;
            EntityType = type;
        }
    }
    
}
