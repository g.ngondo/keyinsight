﻿using System.Collections.Generic;
using System;
using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.models
{
    /// <summary>
    /// Skill & Tool Container
    /// </summary>
    public class BaseInsight
        : BaseEntity, IBaseInsight<BaseSubject>
    {
        public BaseInsight() : base(EntityType.Insight)
        {

        }
        public BaseInsight(InsightType insightType) : base(EntityType.Insight)
        {
            InsightType = insightType;
        }
        public InsightType InsightType { get; }
        public ICollection<BaseSubject> Subjects { get; set; }
        public ICollection<BaseInsight> MatchedInsights { get; set; }
    }
   
}
