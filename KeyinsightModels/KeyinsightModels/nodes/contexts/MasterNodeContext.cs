﻿using System;
using Microsoft.EntityFrameworkCore;
using Keyinsight.core.models;
using System.Collections.Generic;
using Keyinsight.core.interfaces.nodes;
using Keyinsight.core.interfaces.models;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Keyinsight.core.nodes
{
    public class MasterNodeContext : DbContext, IBaseNodeContext, IMasterNodeContext
    {
        public MasterNodeContext(DbContextOptions<MasterNodeContext> options) : base(options)
        {
        }
        public virtual Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            modelBuilder.Entity<BaseInsight>().ToTable("T_Insights");
            modelBuilder.Entity<BaseSubject>().ToTable("T_Subjects");
        }

        public void Initialize()
        {

            DbSet<BaseInsight> insights = this.Set<BaseInsight>();

            for (int i = 0; i < 500; i++)
            {

                BaseInsight insight = new BaseInsight(InsightType.Article)
                {
                    EntityStatus = EntityFlowStatus.Created,
                    EntityPublicId = Guid.NewGuid(),
                    EntityName = "Article",
                    EntityValue = "http://www.marketwatch.com/story/6sense-ranked-a-leader-among-predictive-marketing-analytics-platforms-for-b2b-marketers-by-independent-research-firm-2017-06-16",
                    Subjects = new List<BaseSubject> {
                        new BaseSubject(SubjectType.Category) { EntityName="Category", EntityValue="Big Data", EntityPublicId= Guid.NewGuid()},
                        new BaseSubject(SubjectType.Category) { EntityName="Category", EntityValue="Deep Learning", EntityPublicId= Guid.NewGuid()},
                        new BaseSubject(SubjectType.Category) { EntityName="Category", EntityValue="Web Apps", EntityPublicId= Guid.NewGuid()},
                        new BaseSubject(SubjectType.Category) { EntityName="Category", EntityValue="Motclé", EntityPublicId= Guid.NewGuid()}
                    },
                    MatchedInsights = new List<BaseInsight>
                    {
                        new BaseInsight(InsightType.Person) { EntityName="Person", EntityValue="Jules Vernes", EntityPublicId= Guid.NewGuid()},
                        new BaseInsight(InsightType.Person) { EntityName="Person", EntityValue="Jules Vernes", EntityPublicId= Guid.NewGuid()},
                        new BaseInsight(InsightType.Person) { EntityName="Person", EntityValue="Jules Vernes", EntityPublicId= Guid.NewGuid()}
                    },
                };


                insights.Add(insight);
            }
            this.SaveChanges();
        }

    }
}
