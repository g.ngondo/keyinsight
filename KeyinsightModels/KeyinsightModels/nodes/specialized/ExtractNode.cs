﻿using System;
using Keyinsight.core.interfaces.communications;
using Keyinsight.core.interfaces.engines;
using Keyinsight.core.interfaces.nodes;

namespace Keyinsight.core.nodes
{
    public class ExtractNode : BaseNode, IExtractNode
    {
        public ExtractNode(ExtractNodeContext nodeContext, IWebSocketHandler handler, IWebSocketConnectionManager websocketManager, IEngine engine) 
            : base(nodeContext, handler, websocketManager, engine)
        {
        }
    }
}
