﻿using System;
using Keyinsight.core.interfaces.engines;
using Keyinsight.core.interfaces.nodes;
using Microsoft.Extensions.DependencyInjection;
using Keyinsight.core.interfaces.communications;
using Keyinsight.core.communications;
using Keyinsight.core.communications.messages;

namespace Keyinsight.core.nodes
{
    public class AnalysisNode : BaseNode, IAnalysisNode
    {
        public AnalysisNode(AnalysisNodeContext nodeContext, IWebSocketHandler handler, IWebSocketConnectionManager websocketManager, IEngine engine) 
            : base(nodeContext, handler, websocketManager, engine)
        {
        }
    }
}
