﻿using Keyinsight.core.interfaces.communications;
using Keyinsight.core.interfaces.nodes;
using Keyinsight.core.communications.messages;
using Keyinsight.core.engines;
using Keyinsight.core.interfaces.engines;

namespace Keyinsight.core.nodes
{
    public class MasterNode : BaseNode, IMasterNode
    {
        public MasterNode(MasterNodeContext nodeContext, IWebSocketHandler handler, IWebSocketConnectionManager websocketManager, IEngine engine) 
            : base(nodeContext, handler, websocketManager, engine)
        {
        }
    }
}
