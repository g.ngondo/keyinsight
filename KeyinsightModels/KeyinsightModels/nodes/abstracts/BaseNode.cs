﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Keyinsight.core.interfaces.nodes;
using Keyinsight.core.interfaces.communications;
using Keyinsight.core.interfaces.engines;
using Keyinsight.core.interfaces.repositories;
using Keyinsight.core.repositories;
using Keyinsight.core.interfaces.models;
using Keyinsight.core.models;
using Microsoft.Extensions.DependencyInjection;

namespace Keyinsight.core.nodes
{
    public abstract class BaseNode : IBaseNode
    {
       
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public NodeReplicaType NodeReplicaType { get; set; }
        public NodeStatus NodeStatus { get; set; }
        public NodeType NodeType { get; set; }
        public IBaseNodeContext NodeContext { get; set; }
        public IWebSocketHandler MessageHandler { get; set; }
        public IWebSocketConnectionManager ConnectionManager { get; set; }
        public IEngine Engine { get; set; }
        public IBaseInsightRepository InsightRepository { get; set; }
        public IBaseSubjectRepository SubjectRepository { get; set; }
        public IQueue<IMessage> MessageQueue { get ; set; }
        public SubscriberStatus Status { get; set; }
        
        public BaseNode(IBaseNodeContext nodeContext, IWebSocketHandler handler, IWebSocketConnectionManager websocketManager, IEngine engine)
        {
            NodeContext = nodeContext;
            MessageHandler = handler;
            ConnectionManager = websocketManager;
            Engine = engine;
            InsightRepository = new BaseInsightRepository(nodeContext);
            SubjectRepository = new BaseSubjectRepository(nodeContext);
        }
        public void Notify(IMessage message)
        {
            Status = SubscriberStatus.pending;
            Engine.Notify(message);
            IBaseEntity entity = message.MessageBody;
            switch (entity.EntityType)
            {
                case EntityType.Insight:
                    InsightRepository.Add((BaseInsight)entity);
                    break;
                case EntityType.Subject:
                    SubjectRepository.Add((BaseSubject)entity);
                    break;
            }
        }
      
       
    }
}
