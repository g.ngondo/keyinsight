﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Keyinsight.core.interfaces.nodes;
using Keyinsight.core.nodes;
using Keyinsight.core.interfaces.communications;
using Keyinsight.core.communications;
using Keyinsight.core.communications.messages;
using System;
using Keyinsight.core.interfaces.engines;
using Keyinsight.core.engines;

namespace Keyinsight.core.extensions
{
    public static class ServiceCollectionExtensions
    {
        private static void AddNodeService(this IServiceCollection services, NodeType type, string connectionString)
        {
            services.AddTransient<IWebSocketConnectionManager, WebSocketConnectionManager>();
            services.AddSingleton<IMessageQueue, MessageQueue>();

            switch (type)
            {
                case NodeType.Analysis:
                    services.AddTransient<IWebSocketHandler, AnalysisHandler>();
                    services.AddSingleton<IBaseNode, AnalysisNode>();
                    services.AddSingleton<IEngine, AnalysisEngine>();
                    services.AddDbContext<AnalysisNodeContext>(options =>
                    options.UseSqlServer(connectionString));
                    break;
                case NodeType.Augment:
                    services.AddTransient<IWebSocketHandler, AugmentHandler>();
                    services.AddSingleton<IBaseNode, AugmentNode>();
                    services.AddSingleton<IEngine, AugmentEngine>();
                    services.AddDbContext<AugmentNodeContext>(options =>
                    options.UseSqlServer(connectionString));
                    break;
                case NodeType.Master:
                    services.AddTransient<IWebSocketHandler, MasterHandler>();
                    services.AddSingleton<IBaseNode, MasterNode>();
                    services.AddSingleton<IEngine, MasterEngine>();
                    services.AddDbContext<MasterNodeContext>(options =>
                    options.UseSqlServer(connectionString));
                    
                    break;
                case NodeType.Extract:
                    services.AddTransient<IWebSocketHandler, ExtractHandler>();
                    services.AddSingleton<IBaseNode, ExtractNode>();
                    services.AddSingleton<IEngine, ExtractEngine>();
                    services.AddDbContext<ExtractNodeContext>(options =>
                    options.UseSqlServer(connectionString));
                    break;
            }
        }

       
        public static void InitializeNodeService(this IApplicationBuilder app, string path, NodeType type)
        {
            IBaseNode node;
            IMessageQueue messageQueue;
            IServiceProvider provider = app.ApplicationServices;

            messageQueue = provider.GetService<IMessageQueue>();
            node = provider.GetService<IBaseNode>();
            messageQueue.AddSubscriber(node);

            app.UseMiddleware<WebSocketManagerMiddleware>(node.MessageHandler);
        }

       
        public static void AddMasterNodeService(this IServiceCollection services, string connectionString)
        {
            AddNodeService(services, NodeType.Master, connectionString);
        }
        public static void AddExtractNodeService(this IServiceCollection services, string connectionString)
        {
            AddNodeService(services, NodeType.Extract, connectionString);
        }
        public static void AddAugmentNodeService(this IServiceCollection services, string connectionString)
        {
            AddNodeService(services, NodeType.Augment, connectionString);
        }
        public static void AddAnalysisNodeService(this IServiceCollection services, string connectionString)
        {
            AddNodeService(services, NodeType.Analysis, connectionString);
        }
        

       
        

    }
}
