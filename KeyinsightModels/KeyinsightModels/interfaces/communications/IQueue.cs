﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Keyinsight.core.interfaces.communications
{
    public interface IQueue<T>
    {
        int Size { get; }
        void Enqueue(T obj);
        T Dequeue();
    }
}
