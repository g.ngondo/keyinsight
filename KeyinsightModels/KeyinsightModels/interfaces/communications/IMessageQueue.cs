﻿using Keyinsight.core.interfaces.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Keyinsight.core.interfaces.communications
{
    public interface IMessageQueue : IQueue<IMessage>
    {
        IDictionary<Guid,IMessageSubscriber> Subscribers { get; set; }
        void NotifySubscribers(IMessage message);
        void AddSubscriber(IMessageSubscriber subscriber);
        void RemoveSubscriber(Guid subscriberId);
    }
   
    public interface IMessageSubscriber
    {
        Guid Id { get; set; }
        void Notify(IMessage message);
        SubscriberStatus Status { get; set; }
    }
    public enum SubscriberStatus
    {
        pending,
        onMessageReceived,
    }
}
