﻿using System;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace Keyinsight.core.interfaces.communications
{
    public interface IWebSocketConnectionManager
    {
        ConcurrentDictionary<Guid, WebSocket> Connections { get; set; }
        WebSocket GetConnectionById(Guid id);
        Guid GetConnectionId(WebSocket socket);
        void AddConnection(WebSocket socket);
        Task RemoveConnection(Guid id);
        Guid CreateConnectionId();
    }
}
