﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Keyinsight.core.interfaces.communications
{
    public interface IAnalysisHandler : IWebSocketHandler { }
    public interface IAugmentHandler : IWebSocketHandler { }
    public interface IExtractHandler: IWebSocketHandler { }
    public interface IMasterHandler : IWebSocketHandler { }
}
