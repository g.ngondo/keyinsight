﻿using System.Threading.Tasks;
using System.Net.WebSockets;
using Keyinsight.core.interfaces.models;
using System;
using Keyinsight.core.models;

namespace Keyinsight.core.interfaces.communications
{
    /// <summary>
    /// IConnectionHan generic interface
    /// </summary>
    /// <typeparam name="T">T is an implementation of a websocket protocol</typeparam>
    /// <typeparam name="R">R is the result of the operation on a websocket protocol</typeparam>
    public interface IWebSocketHandler
    {
        IWebSocketConnectionManager ConnectionManager { get; set; }
        //void Initialize(IWebSocketConnectionManager connectionManager);
        void OnConnected(WebSocket socket);
        Task OnDisconnected(WebSocket socket);
        Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer);
        MessageHandlerType HandlerType { get; set; }
        Task SendMessageAsync(WebSocket socket, IMessage message);
        Task SendMessageAsync(Guid id, IMessage message);
        Task SendMessageToAllAsync(IMessage message);
    }

    public enum MessageHandlerType
    {
        MasterHandler,
        ExtractHandler,
        AugmentHandler,
        AnalysisHandler,
    }


}
