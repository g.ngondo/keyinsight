﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Keyinsight.core.interfaces.nodes
{
    public interface IBaseNodeContext
    {
        DbSet<E> Set<E>() where E : class;
        Task<int> SaveChangesAsync();
        void Initialize();
    }

}
