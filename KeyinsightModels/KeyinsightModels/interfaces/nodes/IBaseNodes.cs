﻿
namespace Keyinsight.core.interfaces.nodes
{
    public interface IMasterNode : IBaseNode{ }
    public interface IMasterNodeContext : IBaseNodeContext { }
    public interface IAnalysisNode : IBaseNode { }
    public interface IAnalysisNodeContext : IBaseNodeContext { }
    public interface IAugmentNode : IBaseNode { }
    public interface IAugmentNodeContext : IBaseNodeContext { }
    public interface IExtractNode: IBaseNode { }
    public interface IExtractNodeContext : IBaseNodeContext { }
}
