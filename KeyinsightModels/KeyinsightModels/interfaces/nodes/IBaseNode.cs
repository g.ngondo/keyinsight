﻿using Keyinsight.core.interfaces.communications;
using Keyinsight.core.interfaces.engines;
using Keyinsight.core.interfaces.models;
using Keyinsight.core.interfaces.repositories;
using Microsoft.Extensions.DependencyInjection;
using System;


namespace Keyinsight.core.interfaces.nodes
{
    public interface IBaseNode : IMessageSubscriber
    {
        new Guid Id { get; set; }
        NodeReplicaType NodeReplicaType { get; set; }
        NodeType NodeType { get; set; }
        NodeStatus NodeStatus { get; set; }
        IEngine Engine { get; set; }
        IBaseNodeContext NodeContext { get; set; }
        IWebSocketHandler MessageHandler { get; set; }
        IBaseInsightRepository InsightRepository { get; set; }
        IBaseSubjectRepository SubjectRepository { get; set; }
        IQueue<IMessage> MessageQueue { get; set; }
        

    }

    public enum NodeStatus
    {
        Initialized,
        Synchronized,
        Started,
        Ended,
    }
    public enum NodeReplicaType
    {
        Master,
        Slave
    }

    public enum NodeType
    {
        Master, //Master
        Extract, //Slave
        Augment, //Slave
        Analysis //Slave
    }
}
