﻿
namespace Keyinsight.core.interfaces.models
{
    public interface IBaseSubject : IBaseEntity
    {
        SubjectType SubjectType { get; set; }
    }
    public enum SubjectType
    {
        Category,
        Certification,
        Domain,
        Keyword,
        Language,
        Location,
        Mission,
        Skill,
        Tool,
    }
}
