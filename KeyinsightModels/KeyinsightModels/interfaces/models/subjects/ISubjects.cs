﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Keyinsight.core.interfaces.models
{
    public interface ICategory : IBaseSubject { }
    public interface ICertification : IBaseSubject { }
    public interface ILanguage : IBaseSubject { }
    public interface ILocation : IBaseSubject { }
    public interface IMission : IBaseSubject { }
    public interface ISkill : IBaseSubject { }
    public interface IDomain : IBaseSubject { }
    public interface ITool : IBaseSubject { }
    public interface IKeyword : IEntity, IBaseSubject
    {
        string Text { get; set; }
    }
}
