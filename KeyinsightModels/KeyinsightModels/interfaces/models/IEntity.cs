﻿namespace Keyinsight.core.interfaces.models
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
