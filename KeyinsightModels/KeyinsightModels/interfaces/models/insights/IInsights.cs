﻿namespace Keyinsight.core.interfaces.models
{
    public interface IArticle : IEntity,IBaseInsight<IBaseSubject> { }
    public interface ICv : IEntity, IBaseInsight<IBaseSubject> { }
    public interface IJob : IEntity, IBaseInsight<IBaseSubject> { }
    public interface IOrganization : IEntity, IBaseInsight<IBaseSubject> { }
    public interface IPerson : IEntity, IBaseInsight<IBaseSubject> { }
    public interface ITraining : IEntity, IBaseInsight<IBaseSubject> { }
    
}
