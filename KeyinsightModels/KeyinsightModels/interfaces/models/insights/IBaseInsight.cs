﻿using System.Collections.Generic;

namespace Keyinsight.core.interfaces.models
{
    public interface IBaseInsight<T> : IBaseEntity
        where T : class, IBaseSubject

    {
        InsightType InsightType { get; }
        ICollection<T> Subjects { get; set; }
    }
    public enum InsightType
    {
        Article,
        Cv,
        Job,
        Organization,
        Person,
        Training,
    }
}
