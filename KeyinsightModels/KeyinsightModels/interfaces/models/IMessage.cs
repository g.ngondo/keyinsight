﻿using Keyinsight.core.interfaces.nodes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System.Text;

namespace Keyinsight.core.interfaces.models
{

   
    public interface IMessage
    {
        Guid MessageId { get; set;}
        IMessageDestination MessageTo { get; set; } 
        IBaseEntity MessageBody { get; set; }
    }

    
    public enum IMessageDestination
    {
        MasterNodeRepositories,
        AnalysisNodeRepositories,
        ExtractNodeRepositories,
        AugmentNodeRepositories,
        MasterNodeEngine,
        AnalysisNodeEngine,
        ExtractNodeEngine,
        AugmentNodeEngine,
    }
    
}
