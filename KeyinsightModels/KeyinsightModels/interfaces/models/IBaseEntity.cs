﻿using System;
using System.Collections.Generic;

namespace Keyinsight.core.interfaces.models
{

    public interface IBaseEntity : IEntity
    {
        Guid EntityPublicId { get; set; }
        EntityFlowStatus EntityStatus { get; set; }
        EntityType EntityType { get; }
        DateTime EntityExtractedDate { get; set; }
        DateTime EntityAugmentedDate { get; set; }
        DateTime EntityLoadedDate { get; set; }
        string EntityName { get; set; } //Name of the entity : if it's an article then it will be article
        string EntityValue { get; set; }
    }
    public enum EntityType
    {
        Insight,
        Subject,
    }
    public enum EntityFlowStatus
    {
        Extracted, //From ProviderContext : data is at ProviderEngine
        Created, //From BaseContext : data coming from BaseEngine
        Augmented, //From AugmentContext : data coming from AugmentEngine
        Loaded, //From AnalysisContext : data coming from AnalysisEngine
    }
}
