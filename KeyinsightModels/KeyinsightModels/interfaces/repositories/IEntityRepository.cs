﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Keyinsight.core.interfaces.models;
using System.Threading.Tasks;

namespace Keyinsight.core.interfaces.repositories
{
    public interface IEntityRepository<T>
           where T : IEntity
    {

        RepositoryType RepositoryType { get; set; }
        IEnumerable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> GetAll();
        int Count();
        T GetSingle(int id);
        T GetSingle(Expression<Func<T, bool>> predicate);
        T GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void DeleteWhere(Expression<Func<T, bool>> predicate);
        Task Commit();
    }
    public enum RepositoryType
    {
        Insight, //Insight types
        Article,
        Company,
        Cv,
        Experience,
        Person,
        Training,
        Subject, //Subject types
        Category,
        Keyword,
        Language,
        Location,
        Mission,
        Skill,
        Tool
    }

}
