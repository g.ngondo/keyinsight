﻿using Keyinsight.core.models;

namespace Keyinsight.core.interfaces.repositories
{
    public interface IBaseInsightRepository: IEntityRepository<BaseInsight> {};
    public interface IBaseSubjectRepository : IEntityRepository<BaseSubject> { };
}
