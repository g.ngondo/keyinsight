﻿
namespace Keyinsight.core.interfaces.engines
{
    public interface IAnalysisEngine: IEngine
    {
    }
    public interface IMasterEngine: IEngine { }
    public interface IExtractEngine : IEngine { }
    public interface IAugmentEngine : IEngine { }
}
