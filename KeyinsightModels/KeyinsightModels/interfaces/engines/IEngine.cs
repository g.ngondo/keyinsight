﻿using Keyinsight.core.interfaces.communications;
using System;

namespace Keyinsight.core.interfaces.engines
{
    public interface IEngine : IMessageSubscriber
    {
        new Guid Id { get; set; }
        EngineType Type { get; set; }
    }
    public enum EngineType
    {
        Analysis,
        Augment,
        Extract,
        Master
    }
}
