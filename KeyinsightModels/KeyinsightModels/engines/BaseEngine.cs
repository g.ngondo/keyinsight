﻿using System;
using Keyinsight.core.interfaces.communications;
using Keyinsight.core.interfaces.engines;
using Keyinsight.core.interfaces.models;

namespace Keyinsight.core.engines
{
    /// <summary>
    /// The engine CPU is located on a node
    /// The engine is CPU consuming and handle specialized tasks based on its node
    /// it should work on a another thread than its node
    /// 
    /// </summary>
    public class BaseEngine : IEngine
    {
        public BaseEngine(EngineType type)
        {
            Type = type;
        }
        public Guid Id { get; set; }
        public EngineType Type { get; set; }
        
        public SubscriberStatus Status { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void Notify(IMessage message)
        {
            throw new NotImplementedException();
        }
    }
   
}
