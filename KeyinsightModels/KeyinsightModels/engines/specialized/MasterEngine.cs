﻿using Keyinsight.core.interfaces.engines;
using System;
using System.Collections.Generic;
using System.Text;

namespace Keyinsight.core.engines
{
    public class MasterEngine : BaseEngine
    {
        public MasterEngine() : base(EngineType.Master)
        {
        }
    }
}
