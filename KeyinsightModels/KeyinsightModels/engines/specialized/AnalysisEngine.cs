﻿using Keyinsight.core.interfaces.engines;


namespace Keyinsight.core.engines
{
    public class AnalysisEngine : BaseEngine
    {
        public AnalysisEngine() : base(EngineType.Analysis)
        {
        }
    }
}
