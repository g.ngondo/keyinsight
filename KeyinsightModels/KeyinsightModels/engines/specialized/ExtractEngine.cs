﻿using Keyinsight.core.interfaces.engines;

namespace Keyinsight.core.engines
{
    public class ExtractEngine : BaseEngine
    {
        public ExtractEngine() : base(EngineType.Extract)
        {
        }
    }
}
