﻿using Keyinsight.core.interfaces.engines;

namespace Keyinsight.core.engines
{
    public class AugmentEngine : BaseEngine
    {
        public AugmentEngine() : base(EngineType.Augment)
        {
        }
    }
}
