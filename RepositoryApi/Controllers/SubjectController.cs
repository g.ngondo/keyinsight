﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Keyinsight.core.nodes;
using Keyinsight.core.interfaces.nodes;
using Keyinsight.core.models;
using Keyinsight.core.communications.messages;
using Keyinsight.core.interfaces.models;

namespace RepositoryApi.Controllers
{
    [Route("api/[controller]")]
    public class SubjectController : Controller
    {
        private IMasterNode MasterNode { get; set; }
        public SubjectController(IMasterNode masternode)
        {
            MasterNode = masternode;
        }
        // GET api/Master
        [HttpGet]
        public IEnumerable<BaseEntity> Get()
        {
            return MasterNode.SubjectRepository.GetAll();
        }

        // GET api/Master/Insight/Id
        [HttpGet("{id}")]
        public BaseEntity Get(int id)
        {
            return MasterNode.SubjectRepository.GetSingle(id);
        }

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody]BaseSubject value)
        {
            Message saveToDbMsg = new Message(value, IMessageDestination.MasterNodeRepositories);


            await MasterNode.MessageHandler.SendMessageToAllAsync(saveToDbMsg);
            MasterNode.SubjectRepository.Update(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put([FromBody]BaseSubject value)
        {
            MasterNode.SubjectRepository.Add(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            MasterNode.SubjectRepository.Delete(new BaseSubject()
            {
                Id = id
            });
        }
    }
}
