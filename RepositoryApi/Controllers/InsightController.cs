﻿using Keyinsight.core.communications.messages;
using Keyinsight.core.interfaces.models;
using Keyinsight.core.interfaces.nodes;
using Keyinsight.core.models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repository.api.Controllers
{
    [Route("api/[controller]")]
    public class InsightController : Controller
    {
        private IBaseNode Node { get; set; }
        public InsightController(IBaseNode node)
        {
            Node = node;
        }
        // GET api/Master
        [HttpGet]
        public IEnumerable<BaseEntity> Get()
        {
            return Node.InsightRepository.GetAll();
        }

        // GET api/Master/Insight/Id
        [HttpGet("{id}")]
        public BaseEntity Get(int id)
        {
            return Node.InsightRepository.GetSingle(id);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]BaseInsight value)
        {
            Message message = new Message(value, IMessageDestination.MasterNodeRepositories);
            //MasterNode.Queue.Enqueue(message);
            await Node.MessageHandler.SendMessageToAllAsync(message);
            Node.InsightRepository.Update(value);
            return Ok(value);
        }

        // PUT api/values/5
        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody]BaseInsight value)
        {
            
            Node.InsightRepository.Add(value);
            Message message = new Message(value, IMessageDestination.MasterNodeRepositories);
            
            await Node.MessageHandler.SendMessageToAllAsync(message);
            await Node.InsightRepository.Commit();
            return Ok(message);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Node.InsightRepository.Delete(new BaseInsight()
            {
                Id = id
            });
        }
    }
}
